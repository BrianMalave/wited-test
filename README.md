Wited-test UI admin template

Consideraciones:
- Se escogió un Admin Template gratuito de CoreUI en función a la prueba; por tal motivo, algunas funcionalidades o componentes estan restrigidas, como por ejemplo:

-CDatePicker= para el calendario; en este caso se utilizó @mui/Date Picker.

-CTimePicker: para la selección de horas; se utilizó @mui/Time Picker.

-CMultiSelect: para los inputs de seleccion multiple, en el caso de OA/Cursos inputs; se utilizó @react-select.

-Variant="underline" para los NavLinks es una funcionalidad exclusiva, por lo cual se utilizó variant="pills" en su lugar.

- Se estableció las rutas hacia la pagina 500 en las diferentes views del componente Nav, solo para muestra de funcionalidades.

Muchas gracias por su tiempo en revisar este proyecto. Espero sea de su agrado.
