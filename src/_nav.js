import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilCamera,
  cilNotes,
  cilPencil,
  cilSpeedometer,
  cilStar,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Indicadores',
    to: '/indicadores',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'success',
      text: 'NEW',
    },
  },
  {
    component: CNavTitle,
    name: 'Contenidos',
  },
  {
    component: CNavItem,
    name: 'Asignaturas',
    to: '/contenidos/asignaturas',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Extracurricular',
    to: '/contenidos/extracurricular',
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Secciones',
  },
  {
    component: CNavGroup,
    name: 'Clases en vivo',
    to: '/clases-vivo',
    icon: <CIcon icon={cilCamera} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Particular',
        to: '/clases-vivo/particular',
      },
      {
        component: CNavItem,
        name: 'Institucional',
        to: '/clases-vivo/institucional',
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Editor de clases',
    to: '/editor',
    icon: <CIcon icon={cilPencil} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Crear nueva clase',
        to: '/editor/nueva-clase',
      },
      {
        component: CNavItem,
        name: 'Editar clase',
        to: '/editor/editar-clase',
      },
    ],
  },
]

export default _nav
