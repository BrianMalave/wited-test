import React from 'react'
import { useLocation } from 'react-router-dom'
import { cilSave } from '@coreui/icons';
import CIcon from '@coreui/icons-react';
import routes from '../routes'

import { 
  CBreadcrumb, 
  CBreadcrumbItem,
  CDropdown,
  CButton,
  CDropdownDivider,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle  
} from '@coreui/react'

const AppBreadcrumb = () => {
  const currentLocation = useLocation().pathname

  const getRouteName = (pathname, routes) => {
    const currentRoute = routes.find((route) => route.path === pathname)
    return currentRoute ? currentRoute.name : false
  }

  const getBreadcrumbs = (location) => {
    const breadcrumbs = []
    location.split('/').reduce((prev, curr, index, array) => {
      const currentPathname = `${prev}/${curr}`
      const routeName = getRouteName(currentPathname, routes)
      routeName &&
        breadcrumbs.push({
          pathname: currentPathname,
          name: routeName,
          active: index + 1 === array.length ? true : false,
        })
      return currentPathname
    })
    return breadcrumbs
  }

  const breadcrumbs = getBreadcrumbs(currentLocation)

  return (
    <>
      <CBreadcrumb className="m-0 ms-2">
        <CBreadcrumbItem href="/">Inicio</CBreadcrumbItem>
        {breadcrumbs.map((breadcrumb, index) => {
          return (
            <CBreadcrumbItem
              {...(breadcrumb.active ? { active: true } : { href: breadcrumb.pathname })}
              key={index}
            >
              {breadcrumb.name}
            </CBreadcrumbItem>
          )
        })}
      </CBreadcrumb>
      <CDropdown variant="btn-group">
        <CButton color='primary'>
          <CIcon
            className='me-2'
            icon={cilSave} 
            size="lg"
          />Guardar
        </CButton>
        <CDropdownToggle 
          color='primary' split 
        />
        <CDropdownMenu>
          <CDropdownItem href="#">Guardar como..</CDropdownItem>
          <CDropdownItem href="#">Pegar todo..</CDropdownItem>
          <CDropdownItem href="#">Borrar todo..</CDropdownItem>
          <CDropdownDivider />
          <CDropdownItem href="#">Favoritos</CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    </>
    
  )
}

export default React.memo(AppBreadcrumb)
