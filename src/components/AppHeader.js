import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
  CNav
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilBell, cilMenu } from '@coreui/icons'

import { AppBreadcrumb } from './index'
import { AppHeaderDropdown } from './header/index'
import { logo } from 'src/assets/brand/logo'

const AppHeader = () => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector((state) => state.sidebarShow)

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        <div className='me-3'>
          <CHeaderToggler
            onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
          >
            <CIcon icon={cilMenu} size="xl" />
          </CHeaderToggler>

        </div>
        <CHeaderBrand className="mx-auto d-md-none">
          <CIcon icon={logo} height={48} alt="Logo" />
        </CHeaderBrand>
        <CNav className="d-none d-md-flex me-auto" variant='pills'>
          <CNavItem>
            <CNavLink href="/500" active>
              Academia
            </CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink href="/500">
              CRM
            </CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink href="/500" disabled>
              Equipo
            </CNavLink>
          </CNavItem>
        </CNav>
        <CHeaderNav>
          <CNavItem>
            <CNavLink href="/500">
              <CIcon icon={cilBell} size="xl" />
            </CNavLink>
          </CNavItem>
        </CHeaderNav>
        <CHeaderNav className="ms-3">
          <AppHeaderDropdown />
        </CHeaderNav>
      </CContainer>
      <CHeaderDivider />
      <CContainer fluid>
        <AppBreadcrumb />
      </CContainer>
    </CHeader>
  )
}

export default AppHeader
