import React from 'react';
import Select from 'react-select';

const notaOpcion = [
  { value: '1boa1', label: '1BOA1' },
  { value: '2boa3', label: '2BOA3' },
  { value: '1boa10', label: '1BOA10' }
] 

const MSelectTwo = () => (
  <Select
    color="primary"
    defaultValue={[notaOpcion[2], notaOpcion[3]]}
    isMulti
    name="notas"
    options={notaOpcion}
    className="basic-multi-select"
    classNamePrefix="select"
  />
);
export default MSelectTwo;