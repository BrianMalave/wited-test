import React from 'react';
import Select from 'react-select';

const cursOpcion = [
  { value: 'priBasico', label: 'Primero básico' },
  { value: 'segBasico', label: 'Segundo básico' },
  { value: 'terBasico', label: 'Tercero básico' }
] 

const MSelectOne = () => (
  <Select
    defaultValue={[cursOpcion[2], cursOpcion[3]]}
    isMulti
    name="cursos"
    options={cursOpcion}
    className="basic-multi-select"
    classNamePrefix="select"
  />
);
export default MSelectOne