import React from 'react'
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilBell,
  cilEnvelopeOpen,
  cilSettings,
  cilTask,
  cilUser,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'


const AppHeaderDropdown = () => {
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar color="primary" textColor="white" size='md'>BM</CAvatar>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">
          Cuenta
        </CDropdownHeader>
        <CDropdownItem href="/500">
          <CIcon icon={cilBell} className="me-2" />
          Actualizaciones
          <CBadge color="info" className="ms-2">
            1
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon 
            icon={cilEnvelopeOpen} 
            className="me-2" 
            />Mensajes
          <CBadge color="success" className="ms-2">
            3
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="/500">
          <CIcon 
            icon={cilTask} 
            className="me-2" 
          />Tareas
          <CBadge color="danger" className="ms-2">
            5
          </CBadge>
        </CDropdownItem>
        <CDropdownDivider />
        <CDropdownItem href="/500">
          <CIcon 
            icon={cilUser} 
            className="me-2"
          />Perfil
        </CDropdownItem>
        <CDropdownItem href="/500">
          <CIcon 
            icon={cilSettings} 
            className="me-2"
            />Ajustes
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
