import React from 'react'

const Link = React.lazy(() => import('./views//editorClases/nuevaClase'))

const routes = [
  { path: '/', name: 'Crear nueva clase', element: Link },
  { path: '/indicadores', name: 'Indicadores', element: Link },
  { path: '/contenido', name: 'Contenidos', element: Link },
  { path: '/contenidos/asignaturas', name: 'Asignaturas', element: Link },
  { path: '/contenidos/extracurricular', name: 'Extracurricular', element: Link },
  { path: '/clases-vivo/particular', name: 'Particular', element: Link },
  { path: '/clases-vivo/institucional', name: 'Institucional', element: Link },
  { path: '/editor/nueva-clase', name: 'Crear nueva clase', element: Link },
  { path: '/editor/editar-clase', name: 'Editar clase', element: Link },
]

export default routes
