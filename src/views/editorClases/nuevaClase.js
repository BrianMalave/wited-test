import React from 'react'
import CIcon from '@coreui/icons-react';
import { cilTrash, cilArrowThickFromBottom, cilPaperclip } from '@coreui/icons';
import Muipicker from 'src/components/ccomponents/Muipicker';
import MSelectOne from 'src/components/ccomponents/RSelect';
import MSelectTwo from 'src/components/ccomponents/MSelect';
import { 
  CButton, 
  CCard, 
  CCardBody, 
  CCol, 
  CRow, 
  CForm,
  CFormSelect,
  CFormCheck,
  CFormInput,
  CInputGroup,
  CFormLabel,
  CFormTextarea
} from '@coreui/react'

const nuevaClase = () => {
  return (
    <CRow>
      <CCol xs={12}>
        <h3>Crear clase en vivo</h3>
        
        <CCard className="mb-4">
          <CCardBody>
              <CForm>
                <div className="mb-4">
                  <CFormLabel>
                    <strong>Nombre de la clase</strong>
                  </CFormLabel>
                  <CFormInput
                    type="text"
                    placeholder="Nombre de la clase"
                  />
                </div>
                <div className="mb-4">
                  <CFormLabel>
                    <strong>Descripción de la clase</strong>
                  </CFormLabel>
                  <CFormTextarea
                    placeholder='Descripción de la clase'
                    rows="5" />
                </div>
              </CForm>

              <div className='mb-4'>
                <CFormLabel>
                  <strong>Adjuntar material descargable</strong>
                </CFormLabel>
                  <h6>Nombre del archivo</h6>
                  <CInputGroup>
                    <CFormInput 
                      placeholder="archivo.png"
                    />
                    <CButton type="button" color="primary" variant="outline">
                      <CIcon icon={cilArrowThickFromBottom} size="xl"/>
                    </CButton>
                    <CButton type="reset" color="danger" variant="outline">
                      <CIcon icon={cilTrash} size="xl"/>
                    </CButton>
                  </CInputGroup>
                  <CButton
                    className='mt-3'
                    type="button" 
                    color="primary">
                      <CIcon
                        className='me-2'
                        icon={cilPaperclip} 
                        size="lg" 
                      />Adjuntar
                  </CButton>
              </div>

              <div className='mb-4'>
                <strong>Profesor</strong>
                <CFormSelect
                  className='mt-2'
                  options={[
                    'Seleccionar',
                    { label: 'Jose Perez', value: '1' },
                    { label: 'Brian Malave', value: '2' },
                    { label: 'Daniela Basfi', value: '3', disabled: true }
                  ]}
                />
              </div>

              <div className='mb-4'>
                <strong>Cuenta de Zoom</strong>
                <CFormSelect
                  className='mt-2'
                  options={[
                    'Seleccionar',
                    { label: '@JPerez', value: '1' },
                    { label: '@BMalave', value: '2' },
                    { label: '@DBasfi', value: '3', disabled: true }
                  ]}
                />
              </div>

              <div className='mb-4'>
                <CCol>
                  <CRow className='mb-3'>
                    <strong>Fecha y hora</strong>
                  </CRow>
                  <Muipicker />
                </CCol>
              </div>
              
              <div className='mb-4'>
                <strong>Cantidad máxima de estudiantes</strong>
                <CFormInput
                    className='mt-2'
                    type="text"
                    placeholder="N° de estudiantes"
                  />
              </div>

              <div className='mb-4'>
                <strong>País</strong>
                <CCard className='mt-2'>
                  <CCardBody>
                    <CRow>
                      <CCol>
                        <CFormCheck label="Todos"/>
                        <CFormCheck label="Venezuela"/>
                      </CCol>
                      <CCol>
                        <CFormCheck label="Chile"/>
                        <CFormCheck label="Argentina"/>
                      </CCol>
                      <CCol>
                        <CFormCheck label="México"/>
                        <CFormCheck label="Perú"/>
                      </CCol>
                      <CCol>
                        <CFormCheck label="Colombia"/>
                        <CFormCheck label="Uruguay"/>
                      </CCol>
                    </CRow>

                  </CCardBody>
                </CCard>
              </div>

              <div className='mb-4'>
                <strong>Cursos</strong>
                  <MSelectOne />
              </div>

              <div className='mb-4'>
                <CCard>
                  <CCardBody>
                    <strong>Asignatura</strong>
                    <CFormInput
                        className='mt-2'
                        type="text"
                        placeholder="Nombre de la asignatura"
                      />
                  </CCardBody>
                </CCard>
              </div>

              <div className='mb-4'>
                <strong>OA</strong>
                  <MSelectTwo />
              </div>

              <div className='mb-4'>
                <strong>URL de Zoom</strong>
                <CFormInput
                    className='mt-2'
                    type="text"
                    placeholder="Generado automaticamente por Zoom"
                    disabled
                  />
              </div>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default nuevaClase
