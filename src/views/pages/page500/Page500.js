import React from 'react'
import {
  CButton,
  CCol,
  CContainer,
  CInputGroup,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilCaretLeft } from '@coreui/icons'

const Page500 = () => {
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <span className="clearfix">
              <h1 className="float-start display-3 me-4">500</h1>
              <h4 className="pt-4">Profesor@!, Tenemos problemas! D:</h4>
              <p className="text-medium-emphasis float-start">
              La página que está buscando no está disponible temporalmente,
              pero puedes regresar a través de este botón :D
              </p>
            </span>
            <CInputGroup>
              <CButton color="success" size='lg' component="a" href="/editor/nueva-clase">
                <CIcon 
                  className='me-1'
                  icon={cilCaretLeft} 
                  size="lg"
                  />Regresar
              </CButton>
            </CInputGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Page500
